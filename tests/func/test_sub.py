import pop.hub


def test_pure_python_sub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(python_import="datetime", subname="date")
    assert hub.date.datetime.now()
