def __init__(hub):
    # Replace this location on the hub with a reverse sub parameterized from another sub
    # Make sure the contracts of this directory get added
    hub.pop.sub.dynamic(
        subname="mod",
        sub=hub.exec,
        resolver=hub.tool.mod.init.resolver,
    )
